package main

import(
	"fmt"
	"log"
	"os"
)

func main(){
	
	file, err := os.Create("golang.txt")
	if err != nil{
		log.Fatal("We got error",err)
	}
	
	defer file.Close()
	
	fmt.Fprintf(file, "Devnami Youtube Go Language Tutorial")
}