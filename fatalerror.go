package main

import(
	"log"
	"fmt"
)

func main(){
	fmt.Println("Before Fatal Error")
	
	log.Fatalln("Fatal Error Example")
	
	fmt.Println("After Fatal Error")
}